using System;
using System.Collections.Generic;
using UnityEngine;

namespace Record
{
    [Serializable]
    public class Record: IComparable<Record>
    {
        [SerializeField]
        public int amount;
        [SerializeField]
        public long date;

        public DateTime Date => DateTime.FromFileTimeUtc(date);

        public Record(int amount, DateTime date)
        {
            this.amount = amount;
            this.date = date.ToFileTimeUtc();
        }

        public int CompareTo(Record other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return amount.CompareTo(other.amount);
        }
    }

    [Serializable]
    public class RecordCollection
    {
        [SerializeField]
        public Record[] records;

        public RecordCollection(List<Record> records)
        {
            this.records = records.ToArray();
        }

        public List<Record> ToList()
        {
            return new List<Record>(records);
        }
    }
    
}