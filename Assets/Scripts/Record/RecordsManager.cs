using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Record
{
    public class RecordsManager
    {
        private const string Path = "Records.json";

        private static RecordsManager _instance;

        private List<Record> _records;

        public List<Record> Records => new(_records);

        public static RecordsManager GetInstance()
        {
            return _instance ??= new RecordsManager();
        }

        private RecordsManager()
        {
            ReadRecords();
        }

        private void ReadRecords()
        {
            var fileInfo = new FileInfo(Path);
            if (fileInfo.Exists)
            {
                var jsonString = File.ReadAllText(Path);
                _records = JsonUtility.FromJson<RecordCollection>(jsonString).ToList();
            }
            else
            {
                _records = new List<Record>();
            }
        }

        private void WriteRecords()
        {
            var jsonString = JsonUtility.ToJson(new RecordCollection(_records));
            File.WriteAllText(Path, jsonString);
        }

        public void CheckNewRecord(int amount)
        {
            if (amount == 0) return;
            if (_records.Count == 0 || amount > _records.Max().amount)
            {
                var record = new Record(amount, DateTime.Now);
                _records.Add(record);
                WriteRecords();
            }
        }
    }
}