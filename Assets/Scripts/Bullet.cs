using Enemy;
using Tool;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    [SerializeField] private int lifeTime = 10;
    
    public int DamageAmount { get; set; }
    private void Start()
    {
        StartCoroutine(TimeTool.ExecuteAfterTimeCoroutine(lifeTime, () => Destroy(gameObject)));
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.TryGetComponent(out IOnHitReaction onHit))
        {
            onHit.OnHit(this);
        }
    }
}