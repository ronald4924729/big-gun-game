namespace Enemy
{
    public interface IOnHitReaction
    {
        public void OnHit(Bullet bullet);
    }
}