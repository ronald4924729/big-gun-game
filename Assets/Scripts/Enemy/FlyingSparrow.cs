using System;
using UnityEngine;

namespace Enemy
{
    public class FlyingSparrow: Sparrow
    {
        [SerializeField] private float flySpeed;
        [SerializeField] private float flyHeight;
        [SerializeField] private float flyMagnitude;

        private bool _isUpDirection;
        private Vector3 _startPosition;
        protected override void StartConfiguration(int maxHealth)
        {
            MaxHealth = maxHealth + GameManager.GetInstance().CurrentLevel;
            transform.position += Vector3.up * flyHeight;
            _startPosition = transform.position;
            _isUpDirection = true;
        }

        private void Update()
        {
            if (_isUpDirection)
            {
                transform.position += Vector3.up * (Time.deltaTime * flySpeed);
                if (transform.position.y > _startPosition.y + flyMagnitude)
                {
                    _isUpDirection = false;
                }
            }
            else
            {
                transform.position -= Vector3.up * (Time.deltaTime * flySpeed);
                if (transform.position.y < _startPosition.y)
                {
                    _isUpDirection = true;
                }
            }
        }
    }
}