namespace Enemy
{
    public class Sparrow: AEnemy
    {
        protected override void Death()
        {
            Destroy(gameObject);
        }

        protected override void StartConfiguration(int maxHealth)
        {
            MaxHealth = maxHealth + GameManager.GetInstance().CurrentLevel * 2;
        }
    }
}