using System;
using Tool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class WalkingSparrow: Sparrow
    {
        [SerializeField] private float defaultWalkingSpeed = 4;
        [SerializeField] private float rotatePeriod;
        
        private float _walkingSpeed;

        protected override void StartConfiguration(int maxHealth)
        {
            MaxHealth = maxHealth + GameManager.GetInstance().CurrentLevel;
            _walkingSpeed = defaultWalkingSpeed * Mathf.Pow(1.05f, GameManager.GetInstance().CurrentLevel);
            Rotate();
        }

        private void Update()
        {
            transform.position +=
                transform.rotation * Vector3.forward * (Time.deltaTime * _walkingSpeed);
        }

        private void Rotate()
        {
            transform.Rotate(Vector3.up, Random.Range(-90f, 90f));
            StartCoroutine(TimeTool.ExecuteAfterTimeCoroutine(rotatePeriod, Rotate));
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}