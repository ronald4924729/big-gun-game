using System;
using UnityEngine;

namespace Enemy
{
    public class EnemyHealthBar: MonoBehaviour
    {
        [SerializeField] private Transform backLine;
        [SerializeField] private Transform frontLine;

        private float _fullLineSize;
        private Transform _cameraTransform;

        private void Start()
        {
            _fullLineSize = frontLine.localScale.x;
            if (Camera.main != null) _cameraTransform = Camera.main.transform;
        }

        private void Update()
        {
            transform.LookAt(_cameraTransform);
        }

        public void SetHealth(int health, int maxHealth)
        {
            var lineScale = frontLine.localScale;
            lineScale.x = _fullLineSize * ((float) health / maxHealth);
            frontLine.localScale = lineScale;
        }
    }
}