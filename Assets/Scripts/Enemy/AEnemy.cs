using System;
using System.Linq.Expressions;
using UnityEngine;

namespace Enemy
{
    
    public abstract class AEnemy: MonoBehaviour, IOnHitReaction
    {
        [SerializeField] private int maxHealth;
        [SerializeField] private EnemyHealthBar enemyHealthBar;
        protected int Health { get;  set; }
        public int MaxHealth { get; protected set; }

        public void Start()
        {
            StartConfiguration(maxHealth);
            Health = MaxHealth;
        }

        public void Damage(int damageAmount)
        {
            Health -= damageAmount;
            if (Health <= 0)
            {
                GameManager.GetInstance().KillEnemy(this);
                Death();
            }
            else
            {
                enemyHealthBar.SetHealth(Health, MaxHealth);
            }
        }

        protected abstract void Death();
        protected abstract void StartConfiguration(int maxHealth);
        public void OnHit(Bullet bullet)
        {
            Damage(bullet.DamageAmount);
        }
    }
}