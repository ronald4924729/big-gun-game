using System;
using System.Collections;
using UnityEngine;

namespace Tool
{
    public static class TimeTool
    {
        public static IEnumerator ExecuteAfterTimeCoroutine(float timeInSeconds, Action action)
        {
            yield return new WaitForSeconds(timeInSeconds);
            action();
        }


    }
}