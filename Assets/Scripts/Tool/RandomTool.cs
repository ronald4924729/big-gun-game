using System.Collections.Generic;
using UnityEngine;

namespace Tool
{
    public static class RandomTool
    {

        public static T RandomElement<T>(List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static bool CheckForProbability(float probabilityInPercent)
        {
            return Random.Range(0f, 100f) < probabilityInPercent;
        }
    }
}