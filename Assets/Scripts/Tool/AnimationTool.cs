using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

namespace Tool
{
    public static class AnimationTool
    {

        public static IEnumerator UIPulseAnimationCoroutine(
            float duration,
            float sizeDifference,
            VisualElement visualElement, 
            Action completion = null)
        {
            float time = 0;
            while (time < duration)
            {
                var scale = 1 + (1 - Mathf.Abs(time - duration / 2) / duration * 2) * sizeDifference;
                visualElement.transform.scale = new Vector3(scale, scale, scale);
                
                var delta = Time.deltaTime;
                var t = time + delta > duration ? 1 : time / duration;
                time += delta;
                yield return null;
            }

            visualElement.transform.scale = Vector3.one;
            completion?.Invoke();
        }
    }
}