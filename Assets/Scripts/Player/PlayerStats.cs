using System;
using UI;
using UnityEngine;

namespace Player
{
    public class PlayerStats: MonoBehaviour
    {

        [SerializeField] private int defaultDamageAmount;
        [SerializeField] private float defaultReloadTime;
        
        public int DamageAmount { get; private set; }
        public float ReloadTime { get; private set; }


        private int _unusedLevels;

        private void Start()
        {
            DamageAmount = defaultDamageAmount;
            ReloadTime = defaultReloadTime;
            _unusedLevels = 0;

            GameManager.GetInstance().LevelUp += OnLevelUp;
        }

        private void Update()
        {
            if (_unusedLevels <= 0) return;
            
            if (Input.GetButtonDown("Upgrade Reload"))
            {
                UpgradeReload();
            }

            if (Input.GetButtonDown("Upgrade Damage"))
            {
                UpgradeDamage();
            }
        }

        private void OnLevelUp(int level)
        {
            _unusedLevels++;
        }

        private void UpgradeReload()
        {
            ReloadTime *= 0.9f;
            GameUI.GetInstance().ReloadUpgraded();
            Upgraded();
        }

        private void UpgradeDamage()
        {
            DamageAmount++;
            GameUI.GetInstance().DamageUpgraded();
            Upgraded();
        }

        private void Upgraded()
        {
            _unusedLevels--;
            if (_unusedLevels <= 0)
            {
                GameUI.GetInstance().DisableStats();
            }
        }
    }
}