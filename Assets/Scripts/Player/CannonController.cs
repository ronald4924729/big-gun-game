using Tool;
using UnityEngine;

namespace Player
{
    public class CannonController: MonoBehaviour
    {
        [SerializeField] private Bullet bulletPrefab;
        [SerializeField] private float aimDistance = 10;
        [SerializeField] private float shotPower = 10;
        [SerializeField] private Transform shotAnchor;

        private bool _isReloaded;
        private PlayerStats _playerStats;


        private void Start()
        {
            _isReloaded = true;
            _playerStats = GetComponent<PlayerStats>();
        }

        public void Fire(Ray mouseRay)
        {
            if (!_isReloaded) return;
            var bullet = Instantiate(bulletPrefab);
            var bulletPosition = shotAnchor.position;
            bullet.transform.position = bulletPosition;
            var rigidBody = bullet.GetComponent<Rigidbody>();
            bullet.DamageAmount = _playerStats.DamageAmount;

            var cameraToAnchor = bulletPosition - mouseRay.origin;
            var direction = mouseRay.direction * aimDistance - cameraToAnchor;
            
            rigidBody.AddForce(direction * shotPower);
            
            _isReloaded = false;
            StartCoroutine(TimeTool.ExecuteAfterTimeCoroutine(_playerStats.ReloadTime, () => _isReloaded = true));
        }
    }
}