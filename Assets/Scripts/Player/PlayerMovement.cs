using System;
using UnityEngine;

namespace Player
{
    public class PlayerMovement: MonoBehaviour
    {
        [SerializeField] private float movementSpeed = 10;
        [SerializeField] private float rotationSpeed = 10;
        
        private CharacterController _characterController;
        

        private void Update()
        {
            var vertical = Input.GetAxis("Vertical");
            var horizontal = Input.GetAxis("Horizontal");

            if (vertical != 0)
            {
                transform.position += transform.rotation * Vector3.forward * (Time.deltaTime * movementSpeed * vertical);
            }
            
            if (horizontal != 0)
            {
                transform.Rotate(Vector3.up, Time.deltaTime * rotationSpeed * horizontal);
            }
        }
    }
}