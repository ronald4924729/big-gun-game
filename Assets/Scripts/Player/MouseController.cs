using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(CannonController))]
    public class MouseController: MonoBehaviour
    {
        [SerializeField] private Texture2D aim;
        [SerializeField] private Vector2 size;

        private Vector2 _mousePosition;
        private CannonController _cannonController;
        private Camera _camera;

        private void Start()
        {
            Cursor.visible = false;
            _cannonController = GetComponent<CannonController>();
            _camera = Camera.main;
        }

        private void Update()
        {
            var inputMousePosition = Input.mousePosition;
            _mousePosition.x = inputMousePosition.x;
            _mousePosition.y = Screen.height - inputMousePosition.y;
            _mousePosition -= size / 2 ;
            
            var ray = _camera.ScreenPointToRay(inputMousePosition);
            
            if (Input.GetMouseButtonDown(0))
            {
                _cannonController.Fire(ray);
            }

        }

        private void OnGUI()
        {
            GUI.DrawTexture(new Rect(_mousePosition.x, _mousePosition.y, size.x, size.y), aim, ScaleMode.ScaleToFit, true);
        }
    }
}