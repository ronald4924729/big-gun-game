using Enemy;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Level
{
    public class SpawnZone: MonoBehaviour
    {
        [SerializeField] private Vector2 size;
        [SerializeField] private AEnemy enemy;

        public AEnemy Spawn()
        {
            var position = transform.position;
            var spawnPosition = new Vector3(
                Random.Range(position.x, position.x + size.x), 
                position.y, 
                Random.Range(position.z, position.z + size.y));

            var spawnedEnemy = Instantiate(enemy);
            spawnedEnemy.transform.position = spawnPosition;

            return spawnedEnemy;
        }

        public void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            var position = transform.position;
            Gizmos.DrawLine(position, position + new Vector3(size.x, 0, 0));
            Gizmos.DrawLine(position, position + new Vector3(0, 0, size.y));
            Gizmos.DrawLine(position + new Vector3(size.x, 0, 0), position + new Vector3(size.x, 0, size.y));
            Gizmos.DrawLine(position + new Vector3(0, 0, size.y), position + new Vector3(size.x, 0, size.y));
        }
    }
}