using System;
using System.Collections.Generic;
using System.Linq;
using Record;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

namespace UI
{
    public class MenuUI: MonoBehaviour
    {

        [SerializeField] private VisualTreeAsset recordItemTemplate;
        
        private UIDocument _document;

        private VisualElement _mainMenu;
        private VisualElement _records;
        private VisualElement _titles;

        private ScrollView _recordsScrollView;

        private void Start()
        {
            Cursor.visible = true;
            
            _document = GetComponent<UIDocument>();

            _mainMenu = _document.rootVisualElement.Q<VisualElement>("MainMenu");
            _records = _document.rootVisualElement.Q<VisualElement>("Records");
            _titles = _document.rootVisualElement.Q<VisualElement>("Titles");
            
            _recordsScrollView = _document.rootVisualElement.Q<ScrollView>("RecordsScrollView");
            
            OpenMainMenu();
            ConfigureButtons();
        }

        private void ConfigureButtons()
        {
            _document.rootVisualElement.Q<Button>("NewGameButton").clicked += StartGame;
            _document.rootVisualElement.Q<Button>("RecordsButton").clicked += OpenRecords;
            _document.rootVisualElement.Q<Button>("ExitButton").clicked += Exit;
            _document.rootVisualElement.Q<Button>("TitlesButton").clicked += OpenTitlesMenu;

            _document.rootVisualElement.Q<Button>("RecordsToMenuButton").clicked += OpenMainMenu;
            _document.rootVisualElement.Q<Button>("TitlesToMenuButton").clicked += OpenMainMenu;
            
        }

        private void OpenMainMenu()
        {
            _records.style.display = DisplayStyle.None;
            _titles.style.display = DisplayStyle.None;
            _mainMenu.style.display = DisplayStyle.Flex;
        }
        
        private void OpenTitlesMenu()
        {
            _records.style.display = DisplayStyle.None;
            _titles.style.display = DisplayStyle.Flex;
            _mainMenu.style.display = DisplayStyle.None;
        }
        private void OpenRecords()
        {
            _records.style.display = DisplayStyle.Flex;
            _mainMenu.style.display = DisplayStyle.None;

            _recordsScrollView.Clear();
            
            foreach (var record in RecordsManager.GetInstance().Records.
                         OrderByDescending(record => record.amount).ToList())
            {
                var recordItem = recordItemTemplate.Instantiate();
                recordItem.Q<Label>("RecordAmount").text = record.amount.ToString();
                recordItem.Q<Label>("RecordDate").text = record.Date.ToShortDateString();
                _recordsScrollView.Add(recordItem);
            }
        }
        
        private void StartGame()
        {
            SceneManager.LoadScene(1);
        }


        private void Exit()
        {
            Application.Quit();
        }
    }
}