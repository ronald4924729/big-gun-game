using System;
using Tool;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI
{
    public class GameUI: MonoBehaviour
    {
        private static GameUI _instance;
        
        private UIDocument _document;
        private Label _levelLabel;
        private Label _killsCounterLabel;
        private VisualElement _stats;
        private VisualElement _level;
        private VisualElement _reloadUp;
        private VisualElement _damageUp;
        private VisualElement _killsCounter;
        
        public static GameUI GetInstance()
        {
            return _instance ? _instance : throw new Exception("Game UI instance not found");
        }

        public void DisableStats()
        {
            _stats.style.opacity = new StyleFloat(0.5f);
        }

        public void ReloadUpgraded()
        {
            StartCoroutine(AnimationTool.UIPulseAnimationCoroutine(0.5f, 0.1f, _reloadUp));
        }

        public void DamageUpgraded()
        {
            StartCoroutine(AnimationTool.UIPulseAnimationCoroutine(0.5f, 0.1f, _damageUp));
        }

        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            
            _instance = this;
        }

        private void Start()
        {
            _document = GetComponent<UIDocument>();
            
            _levelLabel = _document.rootVisualElement.Q<Label>("LevelLabel");
            _killsCounterLabel = _document.rootVisualElement.Q<Label>("KillsCounterLabel");
            
            _stats = _document.rootVisualElement.Q<VisualElement>("Stats");
            _reloadUp = _document.rootVisualElement.Q<VisualElement>("ReloadUp");
            _damageUp = _document.rootVisualElement.Q<VisualElement>("DamageUp");
            _level = _document.rootVisualElement.Q<VisualElement>("Level");
            _killsCounter = _document.rootVisualElement.Q<VisualElement>("KillsCounter");
            
            GameManager.GetInstance().LevelUp += UpdateLevel;
            GameManager.GetInstance().EnemyKilled += UpdateKillsCounter;
            
            DisableStats();

        }
        
        private void UpdateLevel(int level)
        {
            _levelLabel.text = level.ToString();
            _stats.style.opacity = new StyleFloat(1f);
            StartCoroutine(AnimationTool.UIPulseAnimationCoroutine(0.5f, 0.1f, _level));
            StartCoroutine(AnimationTool.UIPulseAnimationCoroutine(0.5f, 0.1f, _stats));
        }

        private void UpdateKillsCounter(int count)
        {
            _killsCounterLabel.text = count.ToString();
            StartCoroutine(AnimationTool.UIPulseAnimationCoroutine(0.5f, 0.1f, _killsCounter));
        }
    }
}