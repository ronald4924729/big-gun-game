using UnityEngine;

namespace Booster
{
    public class StopSpawnBooster: ABooster
    {
        [SerializeField] private float stopSpawnTime;
        protected override void Boost()
        {
            GameManager.GetInstance().PauseSpawn(stopSpawnTime);
        }
    }
}