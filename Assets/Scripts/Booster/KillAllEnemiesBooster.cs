namespace Booster
{
    public class KillAllEnemiesBooster: ABooster
    {
        protected override void Boost()
        {
            foreach (var enemy in GameManager.GetInstance().Enemies)
            {
                enemy.Damage(enemy.MaxHealth);
            }
        }
    }
}