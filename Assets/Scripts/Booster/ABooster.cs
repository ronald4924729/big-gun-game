using System;
using Enemy;
using UnityEngine;

namespace Booster
{
    public abstract class ABooster: MonoBehaviour, IOnHitReaction
    {
        private const float RotationSpeed = 10;
        public void OnHit(Bullet bullet)
        {
            Boost();
            Destroy(gameObject);
        }

        public void Update()
        {
            transform.Rotate(Vector3.up, Time.deltaTime * RotationSpeed);
        }

        protected abstract void Boost();
    }
}