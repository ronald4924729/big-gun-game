using System;
using System.Collections;
using System.Collections.Generic;
using Booster;
using Enemy;
using Level;
using Record;
using Tool;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager: MonoBehaviour
{
    private static GameManager _instance;
    
    [SerializeField] private float defaultSpawnPeriod;
    [SerializeField] private int maxEnemiesCount = 10;
    [SerializeField] private int newLevelKills;
    [SerializeField] private float boosterSpawnProbability = 5f;

    [SerializeField] private List<SpawnZone> spawnZones;
    [SerializeField] private List<ABooster> boosters;

    private ISet<AEnemy> _enemies;
    private int _enemiesKilled;
    private float _spawnPeriod;
    private IEnumerator _spawnCoroutine;

    public event Action<int> LevelUp;
    public event Action<int> EnemyKilled; 

    public int CurrentLevel { get; private set; }
    public ISet<AEnemy> Enemies => new HashSet<AEnemy>(_enemies);
    public static GameManager GetInstance()
    {
        return _instance ? _instance : throw new Exception("Game Manager instance not found");
    }

    public void KillEnemy(AEnemy enemy)
    {
        _enemies.Remove(enemy);
        _enemiesKilled++;
        EnemyKilled?.Invoke(_enemiesKilled);

        if (RandomTool.CheckForProbability(boosterSpawnProbability))
        {
            InstantiateBooster(enemy.transform.position);
        }

        if (_enemiesKilled % newLevelKills == 0)
        {
            CurrentLevel++;
            _spawnPeriod *= 0.95f;
            LevelUp?.Invoke(CurrentLevel);
        }
    }

    public void PauseSpawn(float time)
    {
        if (_spawnCoroutine != null)
        {
            StopCoroutine(_spawnCoroutine);
        }

        _spawnCoroutine = TimeTool.ExecuteAfterTimeCoroutine(time, Spawn);
        StartCoroutine(_spawnCoroutine);
    }
    
    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }

        _instance = this;
    }

    private void Start()
    {
        _enemies = new HashSet<AEnemy>();
        _enemiesKilled = 0;
        _spawnPeriod = defaultSpawnPeriod;
        
        Spawn();
    }

    private void Spawn()
    {
        var spawnZone = RandomTool.RandomElement(spawnZones);
        var enemy = spawnZone.Spawn();
        _enemies.Add(enemy);
        
        if (_enemies.Count >= maxEnemiesCount)
        {
            Lose();
            return;
        }

        _spawnCoroutine = TimeTool.ExecuteAfterTimeCoroutine(_spawnPeriod, Spawn);
        StartCoroutine(_spawnCoroutine);
    }

    private void InstantiateBooster(Vector3 position)
    {
        var booster = Instantiate(RandomTool.RandomElement(boosters));
        booster.transform.position = position + new Vector3(0, 3, 0);
    }
    
    private void Lose()
    {
        RecordsManager.GetInstance().CheckNewRecord(_enemiesKilled);
        SceneManager.LoadScene(0);
    }
    
}